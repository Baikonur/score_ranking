﻿using UnityEngine;
using System.Collections;
using PlayRecord;

public class SampleMain : MonoBehaviour
{
    public GameObject scoreRankingObj;
    private RankingManager rankingManager;

	// Use this for initialization
	void Start ()
    {
        rankingManager = scoreRankingObj.GetComponent<RankingManager>();
    }

    //送信ボタン
    public void OnClickSubmit()
    {
        int miliSec = 5000;
        int stage = 0;
        rankingManager.SubmitScore(miliSec, stage, (bool success) => {
            if (success)
            {
                Debug.Log("送信成功");
            }
            else
            {
                Debug.Log("送信失敗");
            }
        });
    }

    //スコアランク表示
    public void OnClickShow()
    {
        Debug.Log("リーダーボード表示");
        rankingManager.ShowLeaderboard(0);
    }
}
