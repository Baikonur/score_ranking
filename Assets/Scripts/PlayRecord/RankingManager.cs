using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections;
using System.Collections.Generic;
using System;
using PlayRecord;

namespace PlayRecord
{
    /**
     * スコアランキングラッパクラス
     * このクラスのメソッドを使ってスコアランクにアクセスする
     * AndroidとiOSでコンパイルオプションでビルド時に切り替える
     * iOSの場合はビルド設定のScripting Define Symbolsに 
     * NO_GPGSを追加する(GooglePlayServiceを使わないようにするための設定)
     */
    public class RankingManager : SingletonMonoBehaviour<RankingManager> 
	{
        [HeaderAttribute("直接リーダーボードIDを記述してください")]
        public bool enable = true;

        //ここにGameCenterリーダーボードのIDを記述
        #if UNITY_IPHONE
        private string[] GameCenter_LeaderboardId =
        {
            "ranking_1",
        };
        #endif
        //ここにGooglePlayGamesリーダーボードのIDを記述
        #if UNITY_ANDROID
        private string[] GooglePlay_LeaderboardId = 
        {
            "CgkIkPPQge8EEAIQAA",
        };
        #endif

        //シングルトン化
        public void Awake()
        {
            if (this != Instance)
            {
                if (this.gameObject != null)
                {
                    Destroy(gameObject);
                }
                else
                {
                    Destroy(this);
                }
                return;
            }

            DontDestroyOnLoad(this.gameObject);
        }

        #if UNITY_IPHONE
        //GameCenterアダプター
        public GameCenterConnector gameCenter = null;
        #elif UNITY_ANDROID
        //GooglePlayアダプター
        public GooglePlayConnector googlePlay = null;
        #endif

        public void Start()
        {
            if (enable)
            {
                #if UNITY_IPHONE
                gameCenter = new GameCenterConnector();
                #elif UNITY_ANDROID
                googlePlay = new GooglePlayConnector();
                #endif
            }
        }

        /**
         * 接続チェック
         */
        public bool IsConnected()
        {
            #if UNITY_IPHONE
            return gameCenter.IsLoggedIn();
            #elif UNITY_ANDROID
            return googlePlay.IsLoggedIn();
            #elif UNITY_EDITOR
            return false;
            #endif
        }

        /**
         * リーダーボードが複数ある場合はIDを取得する処理を記述する
         */
        private string GetLeaderboardID(int stage)
        {
            int index = stage;
            #if UNITY_IPHONE
            return GameCenter_LeaderboardId[index];
            #elif UNITY_ANDROID
            return GooglePlay_LeaderboardId[index];
            #endif
        }

        /**
         * サインイン
         * SumbitScore内で呼び出されるので必須ではない
         * （nameの指定はいらない）
         */
        public bool SignIn(string name, Action<bool>onSignin)
        {
           if (!enable)
           {
                return false;
           }
           if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                return false;
            }

            #if UNITY_IPHONE
			gameCenter.SignIn(name, onSignin);
			#elif UNITY_ANDROID
			googlePlay.SignIn(name, onSignin);
            #endif
            return true;
        }

        /**
         * スコア送信 
         * 必要な引数パラメータなどを決めて記述して下さい
         */ 
        public bool SubmitScore(int scoreMs, int stage, Action<bool>onSubmitScore)
        {
            if (!enable)
            {
                return false;
            }
            if (stage == 0)
            {
                onSubmitScore(true);
                return true;
            }

            #if UNITY_IPHONE
            string rankingId = GetLeaderboardID(stage);
            gameCenter.ReportTimeScore(scoreMs, rankingId, onSubmitScore);//<<- この中でscoreMsの単位変わる
            #elif UNITY_ANDROID
            string rankingId = GetLeaderboardID(stage);
            googlePlay.SendRecord(scoreMs, rankingId, onSubmitScore);
            #endif
            return true;
        }

        /**
         * スコアボード表示
         * 必要な引数パラメータなどを決めて記述して下さい
         */
        public bool ShowLeaderboard(int stage, int options = 0)
        {
            if (!enable)
            {
                return false;
            }
            string rankingId = GetLeaderboardID(stage);

            #if UNITY_IPHONE
            gameCenter.ShowLeaderboard(rankingId);
            #elif UNITY_ANDROID
            googlePlay.ShowLeaderboard(rankingId, 0);
            #endif
            return true;
        }

        /**
         * 未定
         */
        public bool ReportAchievement(string id, int percentage)
        {
            return false;
        }

        /**
         * 未定
         */
        public bool ShowAchievements()
        {
            return false;
        }
    }
}

