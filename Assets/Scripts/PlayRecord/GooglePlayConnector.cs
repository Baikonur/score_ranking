﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_ANDROID
using GooglePlayGames;
#endif
using UnityEngine.SocialPlatforms;

namespace PlayRecord
{
    /**
     * GooglePlayGameのリーダーボードへのアクセスを行うクラス
     */
    public class GooglePlayConnector 
    {
#if UNITY_ANDROID
        private bool active = false;

        public bool IsLoggedIn()
        {
            return active;
        }

        public GooglePlayConnector()
        {
            PlayGamesPlatform.Activate ();
        }

		/**
		 * サインイン
		 */
		public bool SignIn(string name, Action<bool>onSignin)
        {
            Social.localUser.Authenticate((bool success) =>
            {
                active = success;
                if(onSignin != null)
                {
                    onSignin(success);
                }
            });
            return true;
        }        

		/**
		 * リーダーボード表示
		 */
		public void ShowLeaderboard(string id, int option = 0)
		{
            if (active)
            {   
                ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(id);
            }
		}

        public void ShowLeaderboard()
        {
            if (active)
            {
                Social.ShowLeaderboardUI();
            }
        }

        /**
         * スコア送信
         */
        public bool SendRecord(int scoreTimeMs, string boardId, Action<bool>onSend)
		{
            Social.ReportScore(scoreTimeMs, boardId, (bool success) => {
                if(success)
                {
                    //登録成功時の処理
                }
                else
                {
                    //登録失敗時の処理
                }
                if(onSend != null)
                {
                    onSend(success);
                }
            });
			return true;
		}

        public bool SendScore(int score, string boardId, Action<bool>onSend)
        {
            return true;
        }
#endif
    }
}
