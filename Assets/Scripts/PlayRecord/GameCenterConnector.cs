﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;

namespace PlayRecord
{
    /**
     * GameCenterのリーダーボードへのアクセスを行うクラス
     */
    public class GameCenterConnector 
    {
        private Action<bool>callbackOnSignIn = null;
        private Action<bool>callbackOnSubmit = null;
        private bool active = false;

        public bool IsLoggedIn()
        {
            return active;
        }

        /**
         * サインイン
         */
        public bool SignIn(string name, Action<bool>onSignin)
        {
            callbackOnSignIn = onSignin;
            // 認証のため ProcessAuthenticationをコールバックとして登録
            // This call needs to be made before we can proceed to other calls in the Social API
            Social.localUser.Authenticate (ProcessAuthentication);
            return true;
        }
        
        // 認証が完了した時に呼び出される
        // 認証が成功した場合、サーバーからのデータがSocial.localUserにセットされている
        private void ProcessAuthentication (bool success)
        {
            if (success) 
            {
                Debug.Log ("Authenticated, checking achievements");
                active = true;
            } 
            else {
                Debug.Log ("Failed to authenticate");
                active = false;
            }

            if (callbackOnSignIn != null)
            {
                callbackOnSignIn(success);
            }
        }
        
        /**
         * リーダーボードを表示する
         */
        public void ShowLeaderboard(string boardId, int option = 0)
        {
            if (active)
            {
                GameCenterPlatform.ShowLeaderboardUI(boardId, TimeScope.Week);
            }
        }

        /**
         * リーダーボードにスコアを送信する
         * 引数:ミリ秒　ー＞　送信:100分の1秒
         */
        public bool ReportTimeScore(int scoreTimeMs, string boardId, Action<bool>onSubmitScore)
        {
            if (!active)
            {
                return false;
            }
            int gcScore = scoreTimeMs / 10;
            Debug.Log ("スコア " + gcScore + " を次の Leaderboard に報告します。" + boardId);
            callbackOnSubmit = onSubmitScore;        
            Social.ReportScore (gcScore, boardId, success => {
                
                Debug.Log(success ? "スコア報告は成功しました" : "スコア報告は失敗しました");
                if(callbackOnSubmit != null)
                {
                    callbackOnSubmit(success);
                }
            });
            return true;
        }

        //
        public bool ReportIntScore(int scorePoint, string boardId, Action<bool> onSubmitScore)
        {
            //普通の整数スコア（降順）の送信処理を記述してください
            return true;
        }
    }
}