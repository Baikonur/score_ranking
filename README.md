# README #

Unityでスコアランクを実行するクラスのサンプルプロジェクト

Unity5.2以上　GooglePlayGameServices8.4

### What is this repository for? ###

* Unityでスコアランクを実行します
* iOSはGameCenter、AndroidはGooglePlayGameServices、を内部で自動的に切り替えて動作します
* iTunesConnectとGooglePlayGameデベロッパーコンソールでリーダーボードを設定してください

### How do I get set up? ###

* サンプルUnityプロジェクトを開いてビルドしてください
* iOSの場合はNO_GPGSシンボルを設定してください
* RankingManager.csを目的に合わせて加筆修正してください

### 参考サイト ###
* [http://qiita.com/t2low/items/b447f6ed913fabd83b61](http://qiita.com/t2low/items/b447f6ed913fabd83b61)
* [http://stickpan.hatenablog.com/entry/2014/02/24/133411](http://stickpan.hatenablog.com/entry/2014/02/24/133411)
* [http://blog.77jp.net/google-game-service-develop-android](http://blog.77jp.net/google-game-service-develop-android)